FROM ubuntu:latest

ENV HOST_IP=172.17.0.1
ENV PHP_VER=7.2
# Change the default PHP Version as it is updated

RUN apt update
RUN DEBIAN_FRONTEND=noninteractive apt install nginx \
                                                php \
                                                php-cli \
                                                php-common \
                                                php-json \
                                                php-mysql \
                                                php-zip \
                                                php-xml \
                                                php-pgsql \
                                                php-fpm \
                                                php-mbstring \
                                                php-xdebug \
                                                php-curl \
                                                composer \
                                                npm \
                                                iputils-ping \
                                                net-tools \
                                                telnet \
                                                nano \
                                                vim \
                                                certbot \
                                                curl \
                                                language-pack-ru \
                                                postgresql \
                                                mysql-server \
                                                mysql-client \
                                                awscli \
                                                groff \
                                                -y
RUN DEBIAN_FRONTEND=noninteractive apt remove apache2 -y
ENV LANGUAGE ru_RU.UTF-8
ENV LANG ru_RU.UTF-8
ENV LC_ALL ru_RU.UTF-8
RUN locale-gen ru_RU.UTF-8 && dpkg-reconfigure locales
RUN echo Europe/Moscow > /etc/timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
RUN DEBIAN_FRONTEND=noninteractive npm i -g n
RUN DEBIAN_FRONTEND=noninteractive npm i -g node-sass
RUN groupadd -r -g 1000 iddqd && useradd -r -u 1000 -g iddqd iddqd
RUN mkdir home/iddqd && chown -R iddqd:iddqd home/iddqd


# Clean up
RUN DEBIAN_FRONTEND=noninteractive apt autoremove -y
RUN DEBIAN_FRONTEND=noninteractive apt upgrade -y

# Folders & files
RUN mkdir /server
RUN mkdir /server/vhosts
RUN mkdir /server/sites
RUN mkdir /server/sites/default
RUN mkdir /server/config
RUN mkdir /server/config/default
RUN rm -rf /etc/nginx/sites-available/*
RUN rm -rf /etc/nginx/sites-enabled/*
ADD ./atrun.sh /server/config/
ADD ./nginx.conf /server/config/default
ADD ./pg_hba.conf /server/config/default
ADD ./postgresql.conf /server/config/default
ADD ./default.nconf /server/vhosts
ADD ./xdebug.ini /etc/php/7.2/mods-available
RUN mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf_backup
RUN cp /server/config/default/nginx.conf /etc/nginx/nginx.conf
RUN mv /etc/postgresql/10/main/pg_hba.conf /etc/postgresql/10/main/pg_hba.conf_backup
RUN cp /server/config/default/pg_hba.conf /etc/postgresql/10/main/pg_hba.conf
RUN mv /etc/postgresql/10/main/postgresql.conf /etc/postgresql/10/main/postgresql.conf_backup
RUN cp /server/config/default/postgresql.conf /etc/postgresql/10/main/postgresql.conf

# Ports
EXPOSE 80
EXPOSE 443
EXPOSE 3306
EXPOSE 5432

# The last spells
ENV NAME IDDQD

# Let it go
CMD sh /server/config/atrun.sh $PHP_VER $HOST_IP